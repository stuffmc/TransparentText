import SwiftUI

@main
struct TransparentText: App {
    @State var text = "Some Text"
    
    var body: some Scene {
        WindowGroup {
            ZStack {
                Color.yellow
                VStack {
                    TextEditor(text: $text)
                        .foregroundColor(.white)
                        .background(Color.blue.opacity(0.5))
                        .onAppear {
                            UITextView.appearance().backgroundColor = .clear
                        }
                    Text(text)
                }
                .padding()
            }
        }
    }    
}
