# Transparent Text Field 

### Unexpected behavior in iOS 16 when building from Xcode 13.4.1 / iOS SDK 15

Since we cannot use `scrollContentBackground` just yet, we relied on `UITextView.appearance().backgroundColor` up until now and it worked fine.

The attached sample Apps demonstrates how this works.

The `_Xcode14` App can use an `if` to use the correct API, but this can't be used in production until now.

The `TransparentText.swiftpm` can be ran from Xcode 13.4.1 on devices running iOS 15 and 16, the later displaying a black background, because it seems that the `UIAppearance` isn't respected.

Is it the expected behaviour to first deploy our Apps "ready for iOS 16" when Xcode 14 will be released?

Will it be (hopefully) released soon enought that it's before the iOS 16 release?

At the moment our iOS 16 users would see white text on a white background, because our current App displays the text in white, on a green background.
