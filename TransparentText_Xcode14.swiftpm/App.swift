import SwiftUI

@main
struct TransparentText: App {
    @State var text = "Some Text"
    
    var body: some Scene {
        WindowGroup {
            ZStack {
                Color.yellow
                VStack {
                    if #available(iOS 16.0, *) {
                        textField
                            .scrollContentBackground(.hidden)
                            .background(Color.green.opacity(0.5))
                    } else {
                        textField
                            .background(Color.green.opacity(0.5))
                            .onAppear {
                                UITextView.appearance().backgroundColor = .clear
                            }
                    }
                    Text(text)
                }
            }
            .padding()
        }
    }

    var textField: some View {
        TextEditor(text: $text)
            .foregroundColor(.white)
    }
}
